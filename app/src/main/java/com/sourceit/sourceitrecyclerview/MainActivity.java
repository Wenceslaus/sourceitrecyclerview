package com.sourceit.sourceitrecyclerview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerViewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = (RecyclerView) findViewById(R.id.am_recycler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new RecyclerViewAdapter(generateList(), this);
        adapter.setListener(new RecyclerViewAdapter.OnRecyclerItemClickListener() {
            @Override
            public void onItemClickListener(String item, int position) {
                Toast.makeText(MainActivity.this, item, Toast.LENGTH_SHORT).show();
            }
        });
        recyclerView.setAdapter(adapter);
    }

    private ArrayList<String> generateList() {
        ArrayList<String> listElements = new ArrayList<>();
        for (int i = 0; i < 30; i++) {
            listElements.add("Text " + i);
        }
        return listElements;
    }
}
